FROM openshift/origin-base:latest

RUN yum install -y nginx
RUN mkdir -p /var/lib/nginx/run && chmod -R a+w,a+r,a+x /var/lib/nginx && chmod -R a+w,a+r,a+x /var/log/nginx

EXPOSE 80

ADD conf/ /etc/nginx/

CMD ["/usr/sbin/nginx"]

